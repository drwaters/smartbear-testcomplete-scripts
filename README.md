
README for the smartbear-testcomplete-scripts repository

SmartBear TestComplete scripts used to perform automated transactions via TestComplete / TestExecute products

1. Used for periodic synthetic transactions being generated from AWS VM host ec2-44-224-164-39.us-west-2.compute.amazonaws.com
   a. CalVIS registration/download site accessed via an Internet Explorer web browser
   b. CalVIS application run via the CalBAROISClient.exe hybrid client executable with an embedded Internet Explorer web browser

2. Used for Post Implementation Verification/Validation (PIV) activities by the QA team

3. Used for Regression Testing by the QA team

-david.r.waters
